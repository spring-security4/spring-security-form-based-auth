package com.example.springsecurityformbasedauth.securityconfig;

import com.google.common.collect.Sets;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.example.springsecurityformbasedauth.securityconfig.UserPermission.*;

public enum UserRole {
    CUSTOMER(Sets.newHashSet(CUSTOMER_READ)),
    ADMIN(Sets.newHashSet(CUSTOMER_READ,CUSTOMER_WRITE,CUSTOMER_UPDATE,CUSTOMER_DELETE,EMPLOYEE_READ,EMPLOYEE_WRITE,EMPLOYEE_UPDATE,EMPLOYEE_DELETE)),
    MANAGER(Sets.newHashSet(CUSTOMER_READ,CUSTOMER_WRITE,EMPLOYEE_READ,EMPLOYEE_WRITE));

    private final Set<UserPermission> permissions;

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthorities() {
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
