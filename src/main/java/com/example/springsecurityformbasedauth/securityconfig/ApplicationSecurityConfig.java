package com.example.springsecurityformbasedauth.securityconfig;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.concurrent.TimeUnit;

import static com.example.springsecurityformbasedauth.securityconfig.UserRole.*;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;

    /**
     * Form based Auth da biz login page duzeldirik ve asagida yazdigimiz kimi  .loginPage("/login").permitAll() deyirik
     * ve logout olanda ise cookies ve session i silmek ucun ise .clearAuthentication(true).invalidateHttpSession(true)  yaziriq
     * resource papakasinda template papkasina html fayl larini yaziriq ve kodun bunu oxuya bilmesi ucun
     * implementation group: 'org.springframework.boot', name: 'spring-boot-starter-thymeleaf', version: '2.5.0'  bu
     * dependency ni elave edirik eks halda login page ni gormuyub 404 atacaq
     */

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "index", "/css/*", "/js/*").permitAll() // burda her hansisa bir login teleb etmiyecek
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/home",true)
                .passwordParameter("password")
                .usernameParameter("username")
                .and()
                .rememberMe()
                .tokenValiditySeconds((int) TimeUnit.DAYS.toSeconds(14))
                .key("secretkey")
                .rememberMeParameter("remember-me")
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout","GET"))
                .clearAuthentication(true)
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID","remember-me")
                .logoutSuccessUrl("/login");
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails elchin = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("el349")
                .password(passwordEncoder.encode("1234"))
                .authorities(ADMIN.getGrantedAuthorities())
                .build();

        UserDetails ilqar = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("ilqar123")
                .password(passwordEncoder.encode("1234"))
                .authorities(MANAGER.getGrantedAuthorities())
                .build();

        UserDetails lale = User.builder()  // burda yazdigimiz username password ile endpointleri cagiracayiq
                .username("lale123")
                .password(passwordEncoder.encode("1234"))
                .authorities(CUSTOMER.getGrantedAuthorities())
                .build();

        return new InMemoryUserDetailsManager(
                elchin,
                ilqar,
                lale
        );
    }
}
