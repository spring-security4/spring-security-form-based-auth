package com.example.springsecurityformbasedauth.controller;

import com.example.springsecurityformbasedauth.model.Customer;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/customers/")
public class CustomerController {

    @GetMapping("/by-id/{customerId}")
    @PreAuthorize("hasAuthority('customer:read')")
    public Customer getCustomerById(@PathVariable("customerId") Long customerId) {
        return CUSTOMERS.stream()
                .filter(customer -> customerId.equals(customer.getId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Customer " + customerId + " does not exists"));
    }

    @GetMapping("/all")
    // bu sekilde 3 role un da eyni url e accesi oldugunu demek evezine yuxaridaki getCustomerById metodundaki kimi
    // sadece @PreAuthorize("hasAuthority('customer:read')")  bele yazmaq daha qisa olur
    @PreAuthorize("hasAnyRole('ROLE_CUSTOMER','ROLE_MANAGER','ROLE_ADMIN')")
    public List<Customer> getAllCustomer() {
        return CUSTOMERS;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('customer:write')")
    public String createCustomer(@RequestBody Customer customer) {
        customer.setCustomerCode(UUID.randomUUID().toString());
        CUSTOMERS.add(customer);
        System.out.println(CUSTOMERS);
        return "customer created succesfully";
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('customer:write')")
    public String updateCustomer(@PathVariable long id, @RequestBody Customer customerReq) {
        CUSTOMERS.set(Math.toIntExact(id), customerReq);
        return "UPDATED SUCCESFULLY";
    }

    @DeleteMapping("/{customerId}")
    @PreAuthorize("hasAuthority('customer:write')")
    public String deleteCustomer(@PathVariable Long customerId) {
        return "DELETED SUCCESFULLY";
    }

    private static final List<Customer> CUSTOMERS = new ArrayList<>(Arrays.asList(
            new Customer(1L, "Elchin", "Akbarov", UUID.randomUUID().toString()),
            new Customer(2L, "Nigar", "Rehimova", UUID.randomUUID().toString()),
            new Customer(3L, "Saleh", "Asgarov", UUID.randomUUID().toString()),
            new Customer(4L, "Nicat", "Bayramov", UUID.randomUUID().toString()),
            new Customer(5L, "Leman", "Musayeva", UUID.randomUUID().toString())
    ));
}
